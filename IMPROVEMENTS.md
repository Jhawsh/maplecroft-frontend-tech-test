## Issues

- API data (data.json) data is ambiguous with 2 letter country codes. "CH" could refer to many countries.

## Improvements

- Include a search functionality with autocomplete for easier location of countries
- Include a key for the colours
- Implement a popover tooltip on the country so easier to see information
