export interface CountryData {
  [key: string]: {
    score?: number;
    selected: boolean;
    entitled: boolean;
    dataAvailable: boolean;
  };
}
