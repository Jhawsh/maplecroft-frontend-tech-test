export interface Geometry {
  type: string;
  coordinates: number[][][];
}

export interface Properties {
  [key: string]: any;
}

export interface CountryMap {
  type: string;
  properties: Properties;
  geometry: Geometry;
}

export interface CountryMapList {
  type: string;
  features: CountryMap[];
}
