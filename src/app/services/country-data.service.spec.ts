import { TestBed } from '@angular/core/testing';

import { CountryDataService } from './country-data.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CountryData } from '../types/country-data.interface';
import { HttpErrorResponse } from '@angular/common/http';

const MOCK_GET_DATA = {
  AF: {
    selected: false,
    entitled: true,
    dataAvailable: false
  },
  AX: {
    selected: false,
    entitled: false,
    dataAvailable: false
  },
  AL: {
    selected: false,
    entitled: false,
    dataAvailable: false
  },
};

const MOCK_GET_DATA_403 = new HttpErrorResponse({
  status: 403,
  statusText: 'Forbidden'
});

describe('CountryDataService', () => {
  let service: CountryDataService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(CountryDataService);
    httpController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a full set of data from getData', (done) => {
    service.getData().subscribe((res: CountryData) => {
      expect(Object.keys(res).length).toEqual(3);
      done();
    });

    const req = httpController.expectOne({
      method: 'GET',
      url: `./assets/data.json`,
    });

    req.flush(MOCK_GET_DATA);
  });

  it('should throw error when 403 returned from getData', (done) => {
    service.getData().subscribe(
      (res: CountryData) => {
        fail('Should have failed with 403 error');
      },
      (error: HttpErrorResponse) => {
        expect(error.statusText).toBe('Forbidden');
        expect(error.status).toBe(403);
        done();
    });

    const req = httpController.expectOne({
      method: 'GET',
      url: `./assets/data.json`,
    });

    req.error(new ErrorEvent('error'), MOCK_GET_DATA_403);
  });

  it('should return a filtered set of data from getEntitledData', (done) => {
    service.getEntitledData().subscribe((res: CountryData) => {
      expect(Object.keys(res).length).toEqual(1);
      Object.keys(res).forEach((key: string) => {
        expect(res[key].entitled).toBe(true);
      });

      done();
    });

    const req = httpController.expectOne({
      method: 'GET',
      url: `./assets/data.json`,
    });

    req.flush(MOCK_GET_DATA);
  });

  it('should throw error when 403 returned from getEntitledData', (done) => {
    service.getEntitledData().subscribe(
      (res: CountryData) => {
        fail('Should have failed with 403 error');
      },
      (error: HttpErrorResponse) => {
        expect(error.statusText).toBe('Forbidden');
        expect(error.status).toBe(403);
        done();
      });

    const req = httpController.expectOne({
      method: 'GET',
      url: `./assets/data.json`,
    });

    req.error(new ErrorEvent('error'), MOCK_GET_DATA_403);
  });
});
