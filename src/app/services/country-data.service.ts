import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { CountryData } from '../types/country-data.interface';

@Injectable({
  providedIn: 'root'
})
export class CountryDataService {

  private uri = './assets/data.json';

  constructor(private httpClient: HttpClient) {}

  public getData(): Observable<CountryData> {
    return this.httpClient.get<CountryData>(this.uri).pipe(
      catchError(this.handleError)
    );
  }

  public getEntitledData(): Observable<CountryData> {
    return this.getData().pipe(
      map(data => this.filterEntitledData(data)),
    );
  }

  /*
  * In this function we loop through the keys of the objects and only keep
  * the ones where the property 'entitled' is true, we then need to construct
  * the dataset back with the original keys still intact.
  */
  private filterEntitledData(data: CountryData): CountryData {
    return Object.keys(data)
      .filter(key => data[key].entitled)
      .reduce((acc, key) => {
        acc[key] = data[key];
        return acc;
      }, {} as CountryData);
  }

  private handleError(error: any): Observable<never> {
    return throwError(error);
  }
}
